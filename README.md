
# Blockchain Qualification
This is a tool to verify the correct implementation of a blockchain network within the given requirements. Detailed information can be on the corresponding [confluence page](https://pharmatrail.atlassian.net/wiki/spaces/QMS/pages/321093637/Blockchain+Qualification).

This script needs an Kaleido API key to run. The process to obtain it is also documented in the confluence page above.

For now there are no additional parameters to run the script but this is a feature planned for the future. Then this tool can be used as a service.

## Features
- Formatted console output for test results
- Result as JSON formatted array

## Coverage

- Orderer nodes
- Peer nodes
- Organization channels
- Monitoring channel

## TODO

- CAs (Certificate Authorities)


## Setup

Install python requirements

```bash
  pip install -r requirements.txt
```

Run the script

```bash
  python blockchain_qualifier.py 
```


## Documentation

The documentation of the Project can be found 
[here]().

