import json
import requests
import copy
from json.encoder import JSONEncoder
import os
# from pygments import highlight, lexers, formatters




class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def getRequest(path: str):
    url = 'https://console-eu.kaleido.io/api/v1/' + path
    headers = {'Authorization': 'Bearer ' + APIKEY}
    return requests.get(url, headers=headers)


def postRequest(path: str, payload):
    url = 'https://console-eu.kaleido.io/api/v1/' + path
    headers = {'Authorization': 'Bearer ' + APIKEY}
    return requests.post(url, data=payload, headers=headers)
    


def printTest(msg: str, actual: int, required: int):
    global testFailed
    global testCount
    global failedTestCount
    testCount += 1
    if actual >= required:
        print(bcolors.OKGREEN + u'\u2713 ' + bcolors.WARNING + "[" + str(actual) + "/" + str(required) + "] " + bcolors.ENDC + msg)
    else:
        testFailed = True
        failedTestCount += 1
        print(bcolors.FAIL + u'\u2716 ' + bcolors.WARNING + "[" + str(actual) + "/" + str(required) + "] " + bcolors.ENDC + msg)


def initialize():



    global APIKEY
    global CONSORTIA_ID
    global ENVIRONMENT_ID
    APIKEY = "u0ifi4efjg-Mzv1Wslf7dXoy/+xMchmZVh4kqwYcND4CPmQn8kFrto="
    con_id = json.loads(getRequest("consortia").content)[0]["_id"]
    env_amount = len(json.loads(getRequest("consortia/" + con_id + "/environments/").content))

    for env_index in range(env_amount):


        # Requirements to check for
        # These can be parameterized for service scheduling
        minPeers = 2            # minimum amount of peer nodes
        minOrderers = 3         # minimum amount of orderer nodes
        minChannels = 1         # minimum amount of organization channels
        minDefaultChannels = 1  # minimum amount of system monitoring channels

        peerCount = 0
        ordererCount = 0
        channelCount = 0
        defChannelCount = 0


        global testFailed
        global testCount
        global failedTestCount
        testFailed = False
        testCount = 0
        failedTestCount = 0

        # array pos can be looped for all consortiums -> for now we just go through all environments since it's not clear if there is gonna be more than one consortium since the also could be seperated for security reasons
        # get all info / might need the rest later
        CONSORTIA_ID = json.loads(getRequest("consortia").content)[0]["_id"]
        CONSORTIA_NAME = json.loads(getRequest("consortia").content)[0]["name"]
        ENVIRONMENT_ID = json.loads(getRequest(
            "consortia/" + CONSORTIA_ID + "/environments/").content)[env_index]["_id"]
        ENVIRONMENT_NAME = json.loads(getRequest(
            "consortia/" + CONSORTIA_ID + "/environments/").content)[env_index]["name"]
        ENV_CHANNELS = json.loads(getRequest(
            "consortia/" + CONSORTIA_ID + "/environments/" + ENVIRONMENT_ID + "/channels/").content)

        ENV_NODES = json.loads(getRequest(
            "consortia/" + CONSORTIA_ID + "/environments/" + ENVIRONMENT_ID).content)["node_list"]
        NODE_DETAILED = []
        for index in range(len(ENV_NODES)):
            NODE_DETAILED.append(json.loads(getRequest("consortia/" + CONSORTIA_ID +
                                "/environments/" + ENVIRONMENT_ID + "/nodes/" + ENV_NODES[index]).content))
            if NODE_DETAILED[index]["role"] == "peer":
                peerCount += 1
            elif NODE_DETAILED[index]["role"] == "orderer":
                ordererCount += 1
        channelNames = []
        for index in range(len(ENV_CHANNELS)):
            if ENV_CHANNELS[index]["name"] == "default-channel":
                defChannelCount += 1
            else:
                channelNames.append(ENV_CHANNELS[index]["name"])
                channelCount += 1
        print("Consortia: " + CONSORTIA_NAME)
        print("Environment: " + ENVIRONMENT_NAME)
        printTest("Peer Nodes", peerCount, minPeers)
        printTest("Orderer Nodes", ordererCount, minOrderers)
        printTest("Organization Channels", channelCount, minChannels)
        printTest("Monitoring Channel", defChannelCount, minDefaultChannels)

        if testFailed:
            print(bcolors.FAIL + u'\u2716 ' + bcolors.WARNING + "[" + bcolors.FAIL + "Failed" + bcolors.WARNING + "] " + bcolors.WARNING + str(failedTestCount) + bcolors.ENDC + " out of " + bcolors.WARNING + str(testCount) + bcolors.ENDC + " tests have failed")

        else:
            print(bcolors.OKGREEN + u'\u2713 ' + bcolors.WARNING + "[" + bcolors.OKGREEN + "Passed" + bcolors.WARNING + "] " + bcolors.ENDC + "All " + bcolors.WARNING + str(testCount) + bcolors.ENDC + " tests passed")
        print("\n")
        buildJSON(CONSORTIA_NAME, ENVIRONMENT_NAME, peerCount, ordererCount, channelCount, defChannelCount, testCount, failedTestCount)

def buildJSON(orgName: str, envName: str, peerCount: int, ordererCount: int, channelCount: int, monitorCount: int, testCount: int, failedTestCount: int):
    # not sure if an env array is necessary since for now it will only contain one item
    json_data = {} 
    env_array = []
    env = {}

    json_data['organization'] = orgName
    env['name'] = envName
    env['peerCount'] = peerCount
    env['ordererCount'] = ordererCount
    env['channelCount'] = channelCount
    env['monitorCount'] = monitorCount
    env['testCount'] = testCount
    env['failedTestCount'] = failedTestCount

    env_array.append(env)
    json_data['environments'] = env_array


    jsonString = json.dumps(json_data, indent=4)
    print(bcolors.OKCYAN + jsonString)


def main():
    initialize()
    # global APIKEY
    # APIKEY = "u0ifi4efjg-Mzv1Wslf7dXoy/+xMchmZVh4kqwYcND4CPmQn8kFrto="
    # CONSORTIA_ID = json.loads(getRequest("consortia").content)[0]["_id"]
    # env_amount = len(json.loads(getRequest("consortia/" + CONSORTIA_ID + "/environments/").content))

    # print(json.loads(getRequest("consortia").content))
    # print(json.dumps(json.loads(getRequest("consortia/e0f7vw245d/environments/").content), sort_keys=False, indent=4 ))
    # print(json.loads(getRequest("consortia/e0f7vw245d/environments/e0w739zgxp").content)["node_list"])


if __name__ == "__main__":
    main()
